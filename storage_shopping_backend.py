import os
import time

from flask_cors import cross_origin
from flask import Blueprint, request, Response, json
import mariadb
import sys
from dotenv import load_dotenv
import threading
sem = threading.Semaphore()


def get_maria_db_cursor():
    load_dotenv()

    # Connect to MariaDB Platform
    try:
        conn = mariadb.connect(
            user=os.getenv('DB_USER'),
            password=os.getenv('DB_PWD'),
            host=os.getenv('DB_HOST'),
            port=int(os.getenv('DB_PORT')),
            database=os.getenv('DB_NAME')
        )
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)

    # Get Cursor
    cur = conn.cursor()
    return conn, cur


def construct_storage_shopping_blueprint(conn, cur):
    bp = Blueprint('storage_shopping', __name__, url_prefix='/storage_shopping')

    @bp.route('/')
    @cross_origin()
    def index():
        return f"hello from the Storage Shopping API"

    @bp.route("/addItem", methods=["POST"])
    def add_Item():
        try_counter = 0

        while try_counter < 10:
            try:
                new_item = request.json
                if ';' in str(new_item['name']) or ';' in str(new_item['quantity']) or ';' in str(new_item['shopping_list_id']):
                    return Response('Preventing SQL Injection!',  mimetype='application/json')

                sem.acquire()
                print("Inserting these values: " + str(new_item['name']) + ", " + str(new_item['quantity']) + ", "+ str(new_item['shopping_list_id']))
                cur.execute("INSERT INTO items (name, quantity, shopping_list_id) VALUES (?, ?, ?)",
                    (new_item['name'], new_item['quantity'], new_item['shopping_list_id'],))
                conn.commit()

                cur.execute("SELECT id FROM items WHERE name=?", (new_item['name'],))
                sem.release()
                new_id = 0
                for (id) in cur:
                    new_id = id[0]
                print(f"new id: {id}")

                new_item['id'] = new_id
                try_counter = 0
                return Response(json.dumps(new_item),  mimetype='application/json')
            except:
                sem.release()
                try_counter = try_counter + 1
                conn.reconnect()

                time.sleep(0.2)

        return Response('There was an error when connecting to Database!',  mimetype='application/json')

    @bp.route("/addShoppingList", methods=["POST"])
    def add_shopping_list():
        try_counter = 0

        while try_counter < 10:
            try:
                new_item = request.json
                if ';' in str(new_item['name']):
                    return Response('Preventing SQL Injection!',  mimetype='application/json')

                sem.acquire()
                cur.execute(
                    "INSERT INTO shopping_lists (name) VALUES (?)", (new_item['name'],))
                conn.commit()

                cur.execute("SELECT id FROM shopping_lists WHERE name=?", (new_item['name'],))
                sem.release()
                new_id = 0
                for (id) in cur:
                    new_id = id[0]
                print(f"new id for shopping list: {id}")

                new_item['id'] = new_id
                try_counter = 0
                return Response(json.dumps(new_item), mimetype='application/json')
            except:
                sem.release()
                try_counter = try_counter + 1
                conn.reconnect()
                time.sleep(0.2)

        return Response('There was an error when connecting to Database!', mimetype='application/json')

    @bp.route('/changeItemQuantity/<itemId>', methods=["POST"])
    def change_item_quantity(itemId):

        try_counter = 0
        while try_counter < 10:
            try:
                item_to_change = request.json
                if ';' in str(itemId) or ';' in str(item_to_change['quantity']):
                    return Response('Preventing SQL Injection!',  mimetype='application/json')

                print(item_to_change)
                sem.acquire()
                cur.execute(
                    "UPDATE items SET quantity=" + str(item_to_change['quantity']) + " WHERE id = " + str(itemId) + ";"
                )
                conn.commit()
                sem.release()

                try_counter = 0
                print(f"changing quantity to {item_to_change['quantity']} of item with id {itemId}")
                return f"changing quantity to {item_to_change['quantity']} of item with id {itemId}"
            except:
                sem.release()
                try_counter = try_counter + 1
                conn.reconnect()
                time.sleep(0.2)

        return Response('There was an error when connecting to Database!', mimetype='application/json')

    @bp.route('/changeItemFavourite/<itemId>', methods=["POST"])
    def change_item_favourite(itemId):

        try_counter = 0
        while try_counter < 10:
            try:
                item_to_change = request.json
                if ';' in str(itemId) or ';' in str(item_to_change['favourite']):
                    return Response('Preventing SQL Injection!',  mimetype='application/json')

                print(item_to_change)
                sem.acquire()
                cur.execute(
                    "UPDATE items SET favourite=" + str(item_to_change['favourite']) + " WHERE id = " + str(itemId) + ";"
                )
                conn.commit()
                sem.release()

                try_counter = 0
                print(f"changing favourite to {item_to_change['favourite']} of item with id {itemId}")
                return f"changing favourite to {item_to_change['favourite']} of item with id {itemId}"
            except:
                sem.release()
                try_counter = try_counter + 1
                conn.reconnect()
                time.sleep(0.2)

        return Response('There was an error when connecting to Database!', mimetype='application/json')


    @bp.route('/deleteItem/<itemId>', methods=["DELETE"])
    def delete_item(itemId):
        try_counter = 0
        while try_counter < 10:
            try:
                if ';' in str(itemId):
                    return Response('Preventing SQL Injection!',  mimetype='application/json')

                sem.acquire()
                cur.execute(
                    "DELETE FROM items WHERE id = " + str(itemId) + ";"
                )
                conn.commit()
                sem.release()

                try_counter = 0
                return f"delete item with id {itemId}"
            except:
                sem.release()
                try_counter = try_counter + 1
                conn.reconnect()
                time.sleep(0.2)

        return Response('There was an error when connecting to Database!', mimetype='application/json')

    @bp.route('/deleteShoppingList/<itemId>', methods=["DELETE"])
    def delete_shopping_list(itemId):
        try_counter = 0
        while try_counter < 10:
            try:
                if ';' in str(itemId):
                    return Response('Preventing SQL Injection!',  mimetype='application/json')

                sem.acquire()
                cur.execute("DELETE FROM items WHERE shopping_list_id = " + str(itemId) + ";")
                cur.execute(
                    "DELETE FROM shopping_lists WHERE id = " + str(itemId) + ";"
                )
                conn.commit()
                sem.release()

                try_counter = 0
                return f"delete Shopping List with id {itemId}"
            except:
                sem.release()
                try_counter = try_counter + 1
                conn.reconnect()
                time.sleep(0.2)

        return Response('There was an error when connecting to Database!', mimetype='application/json')

    @bp.route('/getItems/<listId>')
    def get_items(listId):
        try_counter = 0
        while try_counter < 10:
            try:
                if ';' in str(listId):
                    return Response('Preventing SQL Injection!',  mimetype='application/json')

                print(listId)
                sem.acquire()
                cur.execute("SELECT * FROM items WHERE shopping_list_id = " + str(listId) + ";")

                items = []
                for (id, name, quantity, favourite, shopping_list_id) in cur:
                    items.append({'id': id, 'name': name, 'quantity': quantity, 'favourite': favourite, 'shopping_list_id': shopping_list_id})
                    print(f"ID: {str(id)}, First Name: {name}, quantity: {str(quantity)}")
                sem.release()
                try_counter = 0
                return Response(json.dumps(items),  mimetype='application/json')
            except:
                sem.release()
                try_counter = try_counter + 1
                conn.reconnect()
                time.sleep(0.2)

        return Response('There was an error when connecting to Database!', mimetype='application/json')

    @bp.route('/getShoppingLists')
    def get_shopping_lists():
        try_counter = 0
        while try_counter < 10:
            try:
                sem.acquire()
                cur.execute("SELECT * FROM shopping_lists")


                items = []
                for (id, name) in cur:
                    items.append({'id': id, 'name': name})
                    print(f"Shopping List: ID: {str(id)}, Name: {name}")
                sem.release()
                try_counter = 0
                return Response(json.dumps(items), mimetype='application/json')
            except:
                sem.release()
                try_counter = try_counter + 1
                conn.reconnect()
                time.sleep(0.2)

        return Response('There was an error when connecting to Database!', mimetype='application/json')

    return bp

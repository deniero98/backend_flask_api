from flask import Flask
from storage_shopping_backend import construct_storage_shopping_blueprint, get_maria_db_cursor
import os
from flask_cors import CORS

ASSETS_DIR = os.path.dirname(os.path.abspath(__file__))
app = Flask(__name__)
CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/')
def index():
    return 'Custom Flask Server!'

conn, cur = get_maria_db_cursor()
app.register_blueprint(construct_storage_shopping_blueprint(conn, cur))


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=4000)

